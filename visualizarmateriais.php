<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Jhonson </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/store.png">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>


    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>

                <a class="navbar-brand" href="index.php">  Jhonson Store </a>
              
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php"> <i class="menu-icon fa fa-home"></i> Inicio </a>
                    </li>
                    <h3 class="menu-title"> Gerenciamento </h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users">

                        </i>Gerenciar Funcionarios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrofuncionario.php">Cadastrar Funcionários</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarfuncionario.php">Visualizar Funcionários</a></li>
                           
                        </ul>

                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart">

                        </i>Gerenciar Pedidos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrarpedido.php">Cadastrar Pedidos</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarpedido.php">Visualizar Pedidos</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-dropbox">

                        </i>Gerenciar Materiais</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-plus-square-o"></i><a href="cadastrarmateriais.php">Cadastrar Materiais</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarmateriais.php">
      Visualizar Materiais</a></li>
                        </ul>
                    </li>


               



                             
                             <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-truck">

                        </i>Gerenciar Fornecedores</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-plus-square-o"></i><a href="cadastrarfornecedor.php">Cadastrar Fornecedor</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarfornecedor.php">
      Visualizar Fornecedores</a></li>

                    </ul>
                </li>  


                    <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>
                        </ul>
                    </li>
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                       

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                     
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Inicio</a></li>
                            <li class="active">Visualizar Funcionário</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">

                                <strong class="card-title">Materiais</strong>

                            </div>
                              
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                         <tr>

                                            <th>Nome Material</th>
                                            <th>Quantidade</th>
                                            <th>Descrição</th>
                                            <th>Tipo Material</th>
                                            <th></th>
                                            <th></th>
                                        </tr>

                                        </thead>

                                          <tbody>
                                        


               <?php  

               include 'backend/Model/bancodedados.php';
                
               $banco =  new bancodedados();


               $select = "SELECT * FROM materiais ORDER BY idMateriais";

               $query = mysqli_query($banco->conectabanco(),$select);

               $dados = mysqli_fetch_assoc($query);
               
               $linhas = mysqli_num_rows($query);


    

                                    


                                        if ($linhas > 0){

                                    do { 

                                        $id =  $dados["idMateriais"];
                                        $fornecedor = $dados["Fornecedor_idFornecedor"];
                                        $nome = $dados["nomeMaterial"];
                                        $quantidade = $dados["quantidadeMaterial"];
                                        $descricao = $dados["descricao"];
                                        $tipo = $dados["tipo"];


                                 echo "   
                                 <tr>
                                   
                                        <td id='nomematerial$id'> $nome </td>
                                        <td id='quantidade$id'> $quantidade </td>
                                        <td id='descricao$id'> $descricao </td>
                                        <td id='tipo$id'> $tipo</td>";







        echo " <td style='width:13%; '><button type='button' style='width: 100%' class='btn btn-outline-primary mb-1 editar' data-toggle='modal' data-target='#mediumModal' id='$id' >Editar</button>
  </td>




<td style='width:13%;'><button type='button' style='width: 100%' class='btn btn-outline-danger excluir' id='$id'>   
                        Excluir</button> </td>

                        </tr>
                        ";
                           
                                     }

                        while ($dados = mysqli_fetch_assoc($query));
                                    }
                                    $banco->fechaBanco();

                        ?>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
  

     <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Edição de Dados</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <p>
                                 
                                 <form id="formEdicao" action="backend/Controller/reload.php" method="POST">

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nome do Material</label></div>
                                        <div class="col-12 col-md-9"><input type="text" name= "nome_material"   placeholder="" class="form-control"><small class="form-text text-muted"> Informe o nome do material a ser cadastrado</small> </small></div>
                                    </div>

                                   
                                     
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">Quantidade de Materiais</label></div>
                                        <div class="col-12 col-md-9"><input type="text" name="quantidade_material" placeholder="" class="form-control"><small class="form-text text-muted">Informe a Quantidade de Materiais</small></div>
                                    </div>
                                 
                                    
                                    <div class="row form-group">
                                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Descrição do Material</label></div>
                                            <div class="col-12 col-md-9"><textarea name="descricao_material" id="textarea-input" rows="5" placeholder="" class="form-control"></textarea>
                                            </div>
                                        </div>

                                     

     <div class="row form-group">
    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tipo do Material</label></div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="tipo_material" placeholder="" class="form-control"><small class="form-text text-muted"> Informe o tipo do material a ser cadastrado</small> </small>
  
                                             
                                             

                                            
                                            </div>
                                            </div>





                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary" id='confirmarEdicao' name='confirmarMaterial'>Editar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

     
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>    
<script src="assets/js/valor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>




<script>
$('.excluir').click(function(){

id = this.id;


    var confirma = confirm("Deseja realmente excluir?");

     if (confirma == true){

   
$.ajax({

    type:'post',
    url: 'backend/Controller/excluirMateriais.php',
    data: {'id': id},
    success: function(data){
        location.reload();
    }

});

}
});

$('.editar').click(function(){
    id = this.id;
    nomeMaterial = $('#nomematerial'+id).text();
    quantidade = $('#quantidade'+id).text();
    descricao = $('#descricao'+id).text();
    tipo_material = $('#tipo'+id).text();

  

  $('#formEdicao').find('input[name="nome_material"]').val(nomeMaterial);
  $('#formEdicao').find('input[name="quantidade_material"]').val(quantidade);
  $('#formEdicao').find('textarea[name="descricao_material"]').val(descricao);
  $('#formEdicao').find('input[name="tipo_material"]').val(tipo_material);
    
   
 $('#confirmarEdicao').click(function() {

    novoNomeMaterial = $('#formEdicao').find('input[name="nome_material"]').val();
    novoQuantidade = $('#formEdicao').find('input[name="quantidade_material"]').val();
    novoDescricao = $('#formEdicao').find('textarea[name="descricao_material"]').val();
    novoTipoMaterial = $('#formEdicao').find('input[name="tipo_material"]').val();

    $.ajax({
        type: 'post',
        url:'backend/Controller/editarMateriais.php',
        data: {'id':id, 
        'nomeMaterial':novoNomeMaterial, 
        'quantidade':novoQuantidade,
        'descricao':novoDescricao,
        'tipoMaterial':novoTipoMaterial},
        success: function(data){
            
        }

    });

    

 });


    

});


</script>

</body>

</html>
