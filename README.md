# System Store
**System Store** É um layout de sistema de administração responsivo.

# Preview

### Screenshot

![](https://i.kym-cdn.com/photos/images/original/000/417/720/484.jpg)



### TOC
- [Built With](#built-with)
- [Changelog](#changelog)
- [Authors](#authors)
- [License](#license)

### Ferramentas Utilizadas

- [Sass](http://sass-lang.com/)
- [Bootstrap](http://getbootstrap.com/)
- [Chart.js](http://www.chartjs.org/)
- [jQuery](https://jquery.com/)
- [Popper.js](https://popper.js.org/)
- [Chosen](https://harvesthq.github.io/chosen/)
- [dataTables](https://datatables.net/)
- [Flot Charts](http://www.flotcharts.org/)
- [gauge.js](http://bernii.github.io/gauge.js/)
- [Peity](http://benpickles.github.io/peity/)
- [Load Google Maps API](https://github.com/yuanqing/load-google-maps-api)
- [JQVMap](https://jqvmap.com/)
- [gmaps](https://hpneo.github.io/gmaps/)
- [Fontawesome](http://fontawesome.io/)


### Versão
#### V 1.0.0

### Autores
[Jeferson Conceição](https://twitter.com/JefersonConcei9)
