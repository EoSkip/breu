﻿CREATE TABLE Funcionario (
idFuncionario INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
nome TEXT  NOT NULL  ,
nascimento DATE  NOT NULL  ,
cpf VARCHAR(14)  NOT NULL  ,
senha VARCHAR(55)  NOT NULL  ,
experiencias TEXT  NOT NULL  ,
setor TEXT  NOT NULL  ,
salario VARCHAR(30)  NULL    ,
PRIMARY KEY(idFuncionario));


CREATE TABLE Fornecedor (
  idFornecedor INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  CNPJ VARCHAR(14)  NOT NULL  ,
  nome_empresa TEXT  NOT NULL  ,
  descricaoempresa TEXT  NOT NULL  ,
  contato VARCHAR(255)  NOT NULL    ,
PRIMARY KEY(idFornecedor));


CREATE TABLE Materiais (
  idMateriais INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Fornecedor_idFornecedor INTEGER UNSIGNED  NOT NULL  ,
  nome TEXT  NOT NULL  ,
  quantidade TEXT  NOT NULL  ,
  descricao VARCHAR(50)  NOT NULL  ,
  tipo TEXT(50)  NOT NULL    ,
PRIMARY KEY(idMateriais));


CREATE TABLE Login (
  idLogin INTEGER UNSIGNED  NOT NULL  ,
  Funcionario_idfuncionario INTEGER UNSIGNED  NOT NULL    ,
PRIMARY KEY(idLogin));


CREATE TABLE Pedido (
  idPedido INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Funcionario_idfuncionario INTEGER UNSIGNED  NOT NULL  ,
  Materiais_idMateriais INTEGER UNSIGNED  NOT NULL  ,
  nome TEXT(50)  NOT NULL  ,
  quantidade INTEGER UNSIGNED  NOT NULL  ,
  descricao TEXT(120)  NOT NULL  ,
  tipo TEXT(50)  NOT NULL    ,
PRIMARY KEY(idPedido, Funcionario_idfuncionario));



